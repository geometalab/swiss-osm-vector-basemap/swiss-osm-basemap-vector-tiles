# Documentation Syntax

### This file explains how the syntax of the documentation parts in the config.toml files works

#### OSM tags

The script that generates the documentation will recognize all character sequences in the form
`key=value` or `key!=value` (negation of `key=value`), where neither the key nor the value contain any whitespace
characters, as OSM tags. From this tag two URLs will be created. One that links to the wiki page of the key and one
that links directly to the wiki page of the tag. Both, the key and value, may only contain ASCII word characters and
non-whitespace characters too as long as they are preceded and succeeded by an ASCII word character. Unlike the key, the
value may be an empty string or an asterisk. If the value is an asterisk or an empty string the second URL
will link to the key's wiki page instead of the value's wiki page. The asterisk means that the corresponding key may be
any value but null or 'no'. If the second text is an empty string this means that the corresponding key is null. \
In order to avoid the creation of a link a space (or any other whitespace character) must be left in front of a  `=` or
`!=` sign.

Examples: \
&nbsp;&nbsp;&nbsp;&nbsp; landuse=forest will become [`landuse`](
https://wiki.openstreetmap.org/wiki/Key:landuse)=[`forest`](https://wiki.openstreetmap.org/wiki/Tag:landuse%3Dforest), \
&nbsp;&nbsp;&nbsp;&nbsp; landuse =forest and landuse = forest will stay `landuse =forest` and `landuse = forest`
respectively. \
&nbsp;&nbsp;&nbsp;&nbsp; landuse= and landuse=* will become [`landuse`](
https://wiki.openstreetmap.org/wiki/Key:landuse)=[`null`](https://wiki.openstreetmap.org/wiki/Key:landuse) and
[`landuse`](
https://wiki.openstreetmap.org/wiki/Key:landuse)=[`*`](https://wiki.openstreetmap.org/wiki/Key:landuse)
respectively

#### Notes

Notes are strings with no special syntax. Character sequences of the form `key=value` will be recognized as OSM tags
(See above for more information). Notes may also be multiline strings but in the output document the line breaks
will be removed, so they will appear on a single line.

Notes should be added to the providers.layers dictionary. They can be added to the columns but this is not recommended.

#### columns

`columns` is a dictionary which is used to document all the columns that a Tegola layer has and to specify what values
can be found in these columns. The columns dictionary may contain strings and two dimensional arrays of strings as
values. \
In a two dimensional array the strings in the inner array are connected by an "and" and the inner arrays themselves are
connected by an "or". \
So `[["waterway=river"], ["water=natural"], ["highway=residential", "access=private"]]` would translate to
waterway=river or water=natural or highway=residential and access=private. Note that the and operator has a higher
precedence than the or operator. So waterway=river or water=natural or (highway=residential and access=private) is
implied.

Every layer must contain a column called geom (is never null) which is where the layer's geometries come from.
In addition to this column the layer may contain more columns which contain more information on a specific geometry in
this layer (for an example documentation see below).


```toml
[[providers.layers]]
geometry_fieldname = "geom"
geometry_type = "LineString"
name = "highway-example"
sql = """
SELECT ST_AsBinary(way) AS geom, highway, access FROM (SELECT
    way,
    CASE WHEN highway IN ('motorway', 'motorway_link') THEN 'motorway'
        WHEN highway IN ('trunk', 'trunk_link') THEN 'trunk'
        ELSE highway END AS highway,
    access
  FROM planet_osm_line
  WHERE highway IN ('motorway', 'motorway_link', 'trunk', 'trunk_link', 'primary', 'secondary')
) AS water_barriers_line WHERE way && !BBOX!"""
```

This query returns the columns `geom`, `highway` and `access`. \
Since `highway` appears in the WHERE clause it makes
sense to specify which values can occur in the columns. In this example highway can be motorway, trunk, primary or
secondary. A value can come from more than one OSM tag (ex. motorway which comes from highway=motorway and
highway=motorway_link). To document this there are two possibilities. The first would be to simply put the information
into a string like this: motorway = "highway=motorway or highway=motorway_link". The second option would be to put the
information into an array like this: motorway = `[["highway=motorway"], ["highway=motorway_link"]]`. The second one is
the preferred option since it is arranged more clearly and easily expandable. \
`access` on the other hand does not appear in the WHERE clause and thus it can be assigned virtually any value. Because
of this it makes more sense to simply link the OSM wiki key.

The documentation for the query above could look like this:

```toml
[providers.layers.columns]
	geom = ""
    access = "for more information see access=*"
	[providers.layers.columns.highway]
		motorway = [["highway=motorway"], ["highway=motorway_link"]]
        trunk = [["highway=trunk"], ["highway=trunk_link"]]
        primary = [["highway=primary"]]
        secondary = [["highway=secondary"]]
```
