

For more detailed information about the Mapbox GL JS Style Specification see
[here](https://docs.mapbox.com/mapbox-gl-js/style-spec/).

Maputnik is an editor for Mapbox GL JS styles.
It has a GUI which makes it very easy to edit the style even if you don't know the Mapbox style spec.
Most common adjustments can be made with the GUI (Graphical User Interface)
but there are some changes that must be made in the _JSON Editor_ part of Maputnik.

Here is a little breakdown of how the Maputnik Editor works:

#### General

[This video](https://youtu.be/XoDh0gEnBQo)
explains the basics of Maputnik well.
For more information on all the different Mapbox layer types see
[here](https://docs.mapbox.com/mapbox-gl-js/style-spec/layers/).

> **NOTE:** The name of the Source is the same as the Tegola map name

#### Filtering columns

If you don't want to treat all features of a Tegola layer the same way, you can add filters.
These filters allow you to select features by their attributes.
A use case could be if you want to display all glaciers.
Glaciers can be found in the water-areas Tegola layer, together with rivers, lakes etc.
You'll probably want to give glaciers a different color than water.
To do this, you can go to _Filter_ -> _Add filter_ and then type in the attribute by which you want to filter,
choose the operator and then the value(s) it should match.

> **NOTE:** If you want an attribute to match multiple values
>you can use the in and !in operators and list multiple values in the right box.
>These values must be separated by a single comma and no whitespaces.

Filtering allows you to simplify Tegola layers with a lot of different contents.
It makes it easier to get a more clearly arranged structure.
It is a really useful feature but if exaggerated it can inflate the style JSON and make it messy.

There is another way to achieve a similar result but it is not really supported by the GUI.
For this approach you will need the _JSON Editor_ which is at the bottom of each layer editing view.

An example for a use case could be if you want to give different road types different widths.
Instead of making a Mapbox layer for every different road type
you can make a single Mapbox layer for multiple road types.


```
"line-width": [
    "case",
    [
        "match",
        ["get", "feature"],
        ["highway_motorway","highway_trunk"],
        true,
        false
    ],
    4,
    [
        "match",
        ["get", "feature"],
        ["highway_service"],
        false,
        true
    ],
    2,
    1
    ]
```

> **NOTE:** This works for colors too

This snippet sets the width of motorways and trunks to 4,
the width of the rest of the roads excluding service roads to 2
and the remaining roads (in this example only service roads) to 1.

* `"case"` means that the result of the first _match_-block that evaluates to true will be applied to a feature.
* `"match"` together with `"get", "feature"` tells us what column will be matched.
  The `"get"` also makes clear that `"feature"` has to be interpreted as an attribute name and not just a value.
* The next line consists of a list of values that will be matched to the previously stated attribute.
* If a value matches the geometry's feature the line the list of values will be evaluated.
  If no value  matches, the second line after the list of values will be evaluated.
  If the evaluated line is `true`, the whole block will evaluate to _true_.
* If the block evaluates to _true_, the value in the first line will be assigned to the feature.
  If the block evaluates to _false_, the second value will be assigned to the feature.
  If there is another _match_-block instead of a value,
  the _match_-block will be evaluated first and then its result will be assigned to the feature.

> **NOTE:** If _true_ comes before _false_
>the _match_-block will evaluate to true if a value matches the feature's attribute
>and if _false_ comes before _true_
>then the _match_-block will evaluate to true if no value matches the feature's attribute.

#### Zoom functions

[This video](https://youtu.be/Z7zD-4uXoRE)
explains zoom functions very well.

#### Data functions

Data functions are similar to zoom functions.
But instead of depending on the zoom of the map, they depend on the feature's data.
A use case of this could be defining the width of waters of the type line.

Data functions are especially useful when an attribute of a feature may have a large amount of values,
for example its width or its area.
These values are not limited to natural numbers which would make it hard to cover all cases.

You can turn some properties into data functions by clicking the bar chart right of the property name.
