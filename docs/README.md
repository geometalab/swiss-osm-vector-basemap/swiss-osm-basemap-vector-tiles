# Instruktion für Map Styling-Autoren

Dies sind Instruktionen für Map Styling-Autoren
zur Entwicklung einer "Swiss OSM Basemap" mit Mapbox Vector Tiles-Technologien.

Websites, die genutzt werden:
* Code Repo:  https://gitlab.com/geometalab/swiss-osm-basemap-vector-tiles/ (Gitlab-Repository-Gruppe von Geometa Lab)
* Style Repo: https://gitlab.com/geometalab/tegola-setup/ (Gitlab-Repository-Gruppe von Geometa Lab)
* Maputnik: https://maputnik.github.io/editor/#15/46.94798/7.44743 (von Freiwilligen gehostete Webapp)


# Einführung

In der Instruktion [hier](https://gitlab.com/geometalab/swiss-osm-basemap-vector-tiles/-/blob/master/README.md)
auf dem ["Swiss OSM Basemap Vector Tiles"](https://gitlab.com/geometalab/swiss-osm-basemap-vector-tiles/-/blob/master/)
Repository gibt es Informationen zum Maputnik-Style-Editor und zum Datenbank-Schema
für einen Karten-Service und -Server "Tegola",
der Vector Tiles (VT) ausliefert.

Hinweis zum Begriff "Style",
zu deutsch sinngemäss "Darstellungsform", kartografisch "Symbologie":
Präziser wäre "Map Style",
also grafische Darstellung - um die es auch hier vor allem geht.
Meist wird aber nur "Style" verwendet.
"Style" ist aber nicht zu verwechseln mit "Data Style",
wie die Konfiguration des Datenbank-Schemas in einigen Vector Tiles-Technologien genannt wird.

Bevor man mit dem Editieren eines Styles im Maputnik-Editor beginnen kann,
lade man eine Style-Datei herunter von
[hier](https://gitlab.com/geometalab/tegola-setup/-/blob/master/html/styles/sample_style.json).
Alternativ kann man auch den Style von
[hier](https://gitlab.com/geometalab/swiss-osm-basemap-vector-tiles/-/blob/master/styles/sample_style.json)
herunterladen.
Beide Styles sind identisch bis auf den Unterschied,
dass beim zweiten die Serveradresse und der Map-Name aus Sicherheitsgründen weggelassen wurde,
da sich dieser auf einem öffentlichen Repository befindet.
Wenn man diesen Style verwendet, muss man auf Zeile 8 diese Informationen noch eintragen.


# Eigener Style

Statt dem "Sample Style" kann man auch von Grund auf einen neuen, eigenen Style erstellen.
Wie das geht, ist in diesem Video erklärt: https://www.youtube.com/watch?v=XoDh0gEnBQo .
Statt am Anfang eine Quelle ("Source") auszuwählen, muss man hier selber eine Quelle erstellen.
Bei Source ID kann man einen beliebigen Namen eintragen.
Beim Source Type wählt man "Vector (XYZ URLs)" und fügt dann bei der "1st Tile URL" eine URL folgenden Formates ein:
TILE-SERVER-ADRESSE/maps/MAP-NAME/{z}/{x}/{y}.vector.pbf.
`Min Zoom` soll auf 0 und `Max Zoom` auf 14 gesetzt sein.
Um die Erstellung der Source fertigzustellen klickt man auf "Add Source".


# Maputnik-Editor

Dann [Maputnik](https://maputnik.github.io/editor/#15/46.94798/7.44743) starten
und oben in der Leiste auf "Open" klicken und dann im Fenster ganz oben auf "Upload Style" gehen;
dann die lokale Datei von "sample style" hochladen.

Maputnik lässt sich recht intuitiv bedienen.
[Hier](https://gitlab.com/geometalab/swiss-osm-basemap-vector-tiles/-/blob/master/docs/mapbox_introduction.md)
ein Paar Informationen dazu.
Hilfreich ist noch die [Mapbox Style Specification](https://docs.mapbox.com/mapbox-gl-js/style-spec/).


# Repositories

Um die Styles an einem Ort zu speichern,
der für alle zugänglich ist,
können diese auf [dieses Repository](https://gitlab.com/geometalab/tegola-setup/-/tree/master/html) hochgeladen werden.
So sind die Styles an einem zentralen Ort und es ist eine Versionskontrolle möglich.
Um einen neuen Style hochzuladen,
muss man im [styles-Ordner](https://gitlab.com/geometalab/tegola-setup/-/tree/master/html/styles)
oben in der Mitte auf das "+" drücken,
um ein Drop-down-Menü zu öffnen.

In diesem Menü gibt es drei Einträge:

1.  "New file": Erstellt eine neue, namenlose Datei.
    Man kann dann der Datei einen Namen (mit Dateiendung) geben, und Inhalt hinzufügen.
    Die "Commit message" und den "Target branch" kann man stehen lassen.
1.  "Upload file": Öffnet einen Upload-Dialog,
    wo man eine Datei hochladen kann.
    Den Namen der Datei kann man jedoch während des Upload-Prozesses nicht ändern.
    Die Commit message und den Target branch kann man stehen lassen.
1.  "New Directory": Öffnet einen Dialog,
    in dem man einen neuen Ordner erstellen kann.
    Man muss hier lediglich den Ordnernamen eingeben und auf "Create directory" drücken.
    Hier kann man dann die Styles auf dieselbe Art hochladen wie oben erläutert.
    Ein Ordner wird standardmässig mit einer .gitkeep-Datei erstellt.
    Diese Datei sorgt dafür, dass der (noch leere) Ordner von der Versionskontrolle beachtet wird.
    Sobald man eine neue Datei in den Ordner getan hat, kann man die .gitkeep-Datei löschen.

Wenn man eine Datei auf dem Repository (GitLab) editieren will,
kann man rechts vom Namen der Datei auf den blauen "Edit"-Knopf drücken und dann die Änderungen vornehmen.
Wenn man fertig ist,
kann man ganz unten auf den grünen "Commit changes"-Knopf drücken,
um die Änderungen zu speichern.

Da das Repository privat ist, wird ein GitLab-Account benötigt.
Neue Accounts kann man hier erstellen: https://gitlab.com/users/sign_up .
Alle, die zum Projekt beitragen wollen, sollen uns bitte ihren Accountnamen schicken,
damit wir sie als "Member" diesem Repository hinzufügen können.

Die Inhalte des Tegola Setup Repositories sollten nicht veröffentlicht werden,
da sie sensible Daten beinhalten können.
Bevor man einen Style veröffentlicht,
soll wieder den oben erwähnte Platzhalter für den Servernamen eingefügt werden.
Der Map-Name gehört nicht zu den sensiblen Daten.
Wenn die Styles auf das Tegola Setup Repository hochgeladen werden,
soll der Servername stehen bleiben.
