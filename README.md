# Swiss OSM Basemap Vector Tiles

This repository contains a configuration file for the [Tegola](https://tegola.io/) vector tiles service.
There is also a [documentation](./docs/layer_documentation.adoc) that explains said file more closely.
In addition to this there is a [sample MapBox style](./styles/sample_style.json) for Tegola.

## Configs and Scripts

### config.toml
The config.toml file consists of the configuration file for Tegola
combined with some documentation elements
which offer more information on the SQL queries that are performed by Tegola.
The base configuration was built from the
[openstreetmap-carto](https://github.com/gravitystorm/openstreetmap-carto) CartoCSS project
using the script in [App-CartoCSS2Tegola](https://github.com/mstock/App-CartoCSS2Tegola/).
For further information see [Tegola OpenStreetMap Carto](https://github.com/mstock/tegola-openstreetmap-carto).
The base configuration was then modified with some documentation elements
(see the [documentation guide](/docs/documentation_guide.md)).

### toml_to_adoc.py
This Python script takes data from the config.toml file and creates a documentation
which provides more information on the layers that are output by Tegola.
This script uses [Jinja](https://palletsprojects.com/p/jinja/) templates to render the asciidoc file.
These templates can be found in the [templates](./templates/) directory.

The resulting asciidoc file can be found [here](./docs/layer_documentation.adoc).

## How to Start Styling

In order to start styling a map simply download the [sample style](./styles/sample_style.json)
and fill in the server address and map name (on line 8).
Open the [Maputnik editor](https://maputnik.github.io/editor/#15/46.94798/7.44743)
and upload the style (click _Open_ and then _Upload_).
You should now be set to edit the Style with Maputnik.
If you want to download the modified style click on _Export_ next to _Upload_ and select _Download_.

### Saving Work

Maputnik saves your style automatically.
It is saved in the web storage of your browser.
This should be safe but it is still recommended to save the style locally every now and then.
In order to share your style with other people or use it in a different browser
you have to export it first.
This is done by clicking _Export_ next to _Open_ and then selecting _Download_ in the dialogue that just opened.

> **NOTE:** If you upload another style in a new window of your browser and reload the old window,
> the old style will not be available anymore.
> If you want to keep the current style, you should always export it before uploading a new one.

### Expanding the Config File

The config.toml file consists out of two parts: the providers and the maps.

The providers part (in the bottom of the file) contains information on the geometry field (field name and geometry type)
as well as the layer's name and the underlying SQL query.
An entry in the providers part looks like this:

```TOML
[[providers.layers]]
geometry_fieldname = "geom"
geometry_type = "LineString"
name = "water-barriers-line"
sql = """
SELECT ST_AsBinary(way) AS geom, "waterway" FROM (SELECT
    way,
    waterway
  FROM planet_osm_line
  WHERE waterway IN ('dam', 'weir', 'lock_gate')
) AS water_barriers_line WHERE way && !BBOX!"""
```

The SQL query must contain at least one !BBOX! token.
Before the query is executed, !BBOX! is substituted by the bounding box of the tile.
The token is used to determine whether a tile is on the screen or not
and thus to decide if a tile should be rendered.
It should appear in the WHERE-clause of the query.

The maps part (in the top of the file) defines the zoom levels of the layers from the providers part.
An entry in the maps part looks like this:

```TOML
[[maps.layers]]
max_zoom = 14
min_zoom = 8
provider_layer = "provider.layer"
```

The min_zoom attribute defines at which zoom level Tegola starts producing tiles with the corresponding layer in  them.
The max_zoom attribute defines at which zoom level Tegola stops generating new data for the tiles.
This means that the layer will still be visible beyond the max_zoom but the data will be "overzoomed".
If the layer should not be visible on a higher zoom level anymore,
the _Max Zoom_ of Maputnik can be set.

### Generating the Layer Documentation

If you want to generate an adoc file from the config file
you just have to run the "toml_to_adoc.py" script.
To run this script [Python 3](https://www.python.org/downloads/) is needed.

If Python 3 is already installed the script can be run with the following command: `python toml_to_adoc.py`.

To export the resulting adoc file to html, asciidoctor is needed
[(See the installation guide)](https://asciidoctor.org/#installation).
If Asciidoctor is already installed the document can be converted to html as described in the
[Asciidoctor documentation](https://asciidoctor.org/docs/convert-documents/#converting-a-document-to-html).


## Authors, Contributors and Acknowledgements 

Authors and Contributors:
* Carlo Del Rossi
* Raphael Das Gupta
* Stefan Keller
* ...

Acknowledgements
* Manfred Stock: https://github.com/mstock/tegola-openstreetmap-carto 
* OSM Swiss Style (Raster): https://github.com/sosm/OSM-Swiss-Style
* ...