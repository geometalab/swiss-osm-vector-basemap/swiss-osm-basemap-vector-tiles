import re
import toml
from pathlib import Path
from jinja2 import Environment, FileSystemLoader, select_autoescape

project_dir = Path(__file__).parent.parent
templates_dir = project_dir / 'templates'
docs_dir = project_dir / 'docs'
OSM_WIKI_URL = 'https://wiki.openstreetmap.org/wiki/'
PATTERN = re.compile('\w+(?:\S\w+)*!?=(?:\*|\w+(?:\S\w+)*|)', flags=re.ASCII)


def render():
    data = toml.load(project_dir / 'config.toml')
    add_zoomlevels_to_layers(data)
    env = Environment(
        loader=FileSystemLoader(searchpath=templates_dir),
    )
    env.filters['linkify_osm_tag'] = linkify_osm_tag
    env.filters['handle_note'] = handle_note
    PROVIDERS_TEMPLATE = env.get_template('providers.adoc.jinja2')
    return PROVIDERS_TEMPLATE.render(
        providers=data['providers'],
        osm_wiki_url=OSM_WIKI_URL
    )


def linkify_osm_tag(osm_tag):
    (key, value), operator = get_key_value_and_operator(osm_tag)
    if value == '':
        value = 'null'
    operator = translate_operator(operator, value)
    key_link = create_link(create_key_url(key), key)
    if value == 'null' or value == '*':
        tag_link = create_link(create_key_url(key), value)
    else:
        tag_link = create_link(create_tag_url(key, value), value)
    return f'{key_link}{operator}{tag_link}'


def handle_note(note):
    note = note.replace('\n', '')
    start = 0
    string = ''
    for match in re.finditer(PATTERN, note):
        string += note[start:match.start()] + linkify_osm_tag(note[match.start():match.end()])
        start = match.end()
    string += note[start:]
    return string


def translate_operator(operator, value):
    if value != 'null':
        return operator
    else:
        if operator == '=':
            return ' is '
        else:
            return ' is not '


def get_key_value_and_operator(osm_tag):
    if '!=' in osm_tag:
        return osm_tag.split('!='), '!='
    else:
        return osm_tag.split('='), '='


def create_link(url, text):
    return f'link:{url}[+{text}+]'


def create_key_url(key):
    return f'{OSM_WIKI_URL}Key:{key}'


def create_tag_url(key, value):
    return f'{OSM_WIKI_URL}Tag:{key}%3D{value}'


def add_zoomlevels_to_layers(data):
    for tegola_provider in data['providers']:
        provider_name = tegola_provider['name']
        for layer in tegola_provider['layers']:
            tegola_map = get_tegola_map(data, f'{provider_name}.{layer["name"]}')
            layer['min_zoom'] = tegola_map['min_zoom']
            layer['max_zoom'] = tegola_map['max_zoom']


def get_tegola_map(data, name):
    layer, = (
        layer
        for tegola_map in data['maps']
        for layer in tegola_map['layers']
        if layer['provider_layer'] == name
    )
    return layer


if __name__ == '__main__':
    with open(docs_dir / 'layer_documentation.adoc', 'w') as file:
        file.write(render())
